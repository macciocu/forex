import React from 'react';
import './App.css';

const RESTAPI_ENDPOINT = 'http://0.0.0.0:80/api';
const RESTAPI_ENDPOINT_GET_TRADES = `${RESTAPI_ENDPOINT}/get_trades`;
const RESTAPI_ENDPOINT_GET_CCY_CONV = `${RESTAPI_ENDPOINT}/get_currency_conversion_rate`;
const RESTAPI_ENDPOINT_POST_TRADE = `${RESTAPI_ENDPOINT}/create_trade`;

export default class App extends React.Component {
  constructor() {
    super();

    this.VIEWS = {
      TRADES: 1,
      CREATE_TRADE: 2,
    };

    this.state = {
      view: this.VIEWS.TRADES,
    };
  }

  showCreateTradeView = () => {
    this.setState({
      view: this.VIEWS.CREATE_TRADE,
    });
  };

  showTradesView = () => {
    this.setState({
      view: this.VIEWS.TRADES,
    });
  };

  render() {
    if (this.state.view === this.VIEWS.TRADES) {
      return (
        <div className="App">
          <Header title="Booked Trades" />
          <Trades
            tradeRows={this.state.tradeRows}
            createTradeCallback={this.showCreateTradeView}
          />
        </div>
      );
    } else if (this.state.view === this.VIEWS.CREATE_TRADE) {
      return (
        <div className="App">
          <Header title="New Trade" />
          <CreateTrade showTradesViewCallback={this.showTradesView} />
        </div>
      );
    } else {
      return 'Oops! Something went wrong, this view is not supported';
    }
  }
}

class Trades extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tradeRows: [],
    };
  }

  async componentDidMount() {
    let trades = await httpGetReq(RESTAPI_ENDPOINT_GET_TRADES);
    if (trades !== null) {
      let rows = [];
      for (let i = 0; i < trades.length; ++i) {
        const trade = trades[i];
        rows.push(
          <tr key={i}>
            <td>{trade.sell_currency}</td>
            <td>{trade.sell_amount}</td>
            <td>{trade.buy_currency}</td>
            <td>{trade.buy_amount}</td>
            <td>{trade.rate}</td>
            <td>{trade.date_booked}</td>
          </tr>,
        );
      }
      this.setState({
        tradeRows: rows,
      });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div
          style={{
            display: 'flex-block',
            flexDirection: 'column',
          }}
        >
          <div style={{ background: 'blue' }}>
            <button
              onClick={this.props.createTradeCallback}
              className="green"
              style={{ float: 'right' }}
            >
              New Trade
            </button>
          </div>
          <div>
            <table style={{ width: '100%' }}>
              <tbody>
                <tr>
                  <th>Sell CCY</th>
                  <th>Sell Amount</th>
                  <th>Buy CCY</th>
                  <th>Buy Amount</th>
                  <th>Rate</th>
                  <th>Date Booked</th>
                </tr>
                {this.state.tradeRows}
              </tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

class CreateTrade extends React.Component {
  constructor(props) {
    super(props);
    this.sellCurrency = '';
    this.buyCurrency = '';
    this.state = {
      sellAmount: 1,
      buyAmount: 0,
      rate: '...',
    };
  }

  setSellCurrencyCallback = async currency => {
    this.sellCurrency = currency;
    this.computeForexRate();
  };

  setBuyCurrencyCallback = async currency => {
    this.buyCurrency = currency;
    this.computeForexRate();
  };

  async computeForexRate() {
    if (this.sellCurrency === '' || this.buyCurrency === '') {
      return;
    }
    const data = await httpGetReq(
      `${RESTAPI_ENDPOINT_GET_CCY_CONV}?` +
        `ccy_from=${this.sellCurrency}&ccy_to=${this.buyCurrency}`,
    );

    if (data.success == false) {
      alert(data.error.type);
    } else {
      const rate = data.rates[this.buyCurrency];
      this.setState({
        rate: rate,
        buyAmount: Math.round(rate * this.state.sellAmount * 100) / 100,
      });
    }
  }

  handleSellAmountChange = e => {
    if (this.sellCurrency === '' || this.buyCurrency === '') {
      this.setState({
        sellAmount: e.target.value,
      });
    } else {
      this.setState({
        sellAmount: e.target.value,
        buyAmount: Math.round(e.target.value * this.state.rate * 100) / 100,
      });
    }
  };

  handleCreateNewTrade = () => {
    if (
      httpPostReq(RESTAPI_ENDPOINT_POST_TRADE, {
        sell_currency: this.sellCurrency,
        sell_amount: this.state.sellAmount,
        buy_currency: this.buyCurrency,
        buy_amount: this.state.buyAmount,
        rate: this.state.rate,
      })
    ) {
      this.props.showTradesViewCallback();
    }
  };

  render() {
    return (
      <div className="createTradeContainer">
        <div className="inputs">
          <h3>Sell Currency</h3>
          <CurrencySelector
            selectCurrencyCallback={this.setSellCurrencyCallback}
          />
          <h3>Sell Amount</h3>
          <input
            type="number"
            value={this.state.sellAmount}
            onChange={this.handleSellAmountChange}
          />
          <br />
          <button
            disabled={this.state.rate === '...' ? true : false}
            className="green"
            onClick={this.handleCreateNewTrade}
          >
            Create
          </button>
        </div>
        <div style={{ width: '100%' }}>
          <h3>Rate</h3>
          <b>&#x25BA;&nbsp;{this.state.rate}&nbsp;&#x25BA;</b>
        </div>
        <div className="inputs">
          <h3>Buy Currency</h3>
          <CurrencySelector
            selectCurrencyCallback={this.setBuyCurrencyCallback}
          />
          <h3>Buy Amount</h3>
          <input type="number" value={this.state.buyAmount} disabled={true} />
          <br />
          <button className="red" onClick={this.props.showTradesViewCallback}>
            Cancel
          </button>
        </div>
      </div>
    );
  }
}

class CurrencySelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currencySelect: '',
    };
  }

  handleCurrencySelect = async e => {
    this.setState({
      currencySelect: e.target.value,
    });
    this.props.selectCurrencyCallback(e.target.value);
  };

  render() {
    return (
      <select
        value={this.state.currencySelect}
        onChange={this.handleCurrencySelect}
        selected=""
      >
        <option value="" disabled hidden></option>
        <option value="EUR">EUR</option>
        <option value="USD">USD</option>
        <option value="CNY">CNY</option>
        <option value="GBP">GBP</option>
        <option value="JPY">JPY</option>
      </select>
    );
  }
}

function Header(props) {
  return (
    <React.Fragment>
      <h1>FOREX</h1>
      <h2>{props.title}</h2>
    </React.Fragment>
  );
}

/**
 * Performs HTTP GET Request.
 * @param endpoint (str): Endpoint URI.
 * @return HTTP GET request data (extracted from response), returns `null`
 *  upon failure, logs error message to console.
 */
async function httpGetReq(endpoint) {
  const data = await fetch(endpoint)
    .then(function(resp) {
      if (resp.status !== 200) {
        console.error(
          `Failed to perform HTTP GET request on: ${endpoint}; ` +
            `resp.status == ${resp.status}`,
        );
        return null;
      }
      return resp.json().then(function(data) {
        return data;
      });
    })
    .catch(function(err) {
      console.error(
        `Failed to perform HTTP GET request on: ${endpoint}; ` +
          `error message: ${err}`,
      );
      return null;
    });
  return data;
}

/**
 * Performs HTTP POST Request.
 * @param endpoint (str): Endpoint URI.
 * @param payload (dict): Holds data to be posted.
 * @return True upon success, false if failure (logs error message to console).
 */
async function httpPostReq(endpoint, payload) {
  fetch(endpoint, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  })
    .then(resp => {
      if (resp.status !== 200) {
        console.error(
          `Failed to perform HTTP POST request on: ${endpoint}; ` +
            `resp.status == ${resp.status}`,
        );
        return false;
      }
    })
    .catch(err => {
      console.error(
        `Failed to perform HTTP GET request on: ${endpoint}; ` +
          `error message: ${err}`,
      );
      return false;
    });
  return true;
}
