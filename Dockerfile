# intermediate image
FROM node:alpine as REACT_BUILD
WORKDIR /app
COPY dashboard .
RUN yarn install && yarn build

# final image
FROM nginx:1.17.8
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/forex.conf /etc/nginx/conf.d/
COPY --from=REACT_BUILD /app/build /usr/share/nginx/html
