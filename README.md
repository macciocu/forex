# README #

## Introduction

This project contains a full stack, dockerized web application for creating and
storing foreign exchange trades. Note that in order to use this application,
you need to export the **FOREXIO_API_KEY** variable, for more info see the
**Getting Started** section.

## Project Structure

```

├── ./dashboard ... Web application (frontend)
├── ./database .... Database microservice (backend)
├── Dockerfile .... Web app. server microserverice (backend)
├── ./nginx ....... Web app. server microservice configuration (backend)
└── ./restapi ..... WSGI Gateway + REST API (backend)
```

Note: nginx Dockerfile is located at project root directory as it needs to copy
the dashboard application into the image (Copying is not allowed outside of
Docker's build context).

## System Architecture

```
      0.0.0.0:80
  ^ + (browser)
  | |
  | v
+-+-+----------------+
|   NGINX            |
|   Web App Server   |
|                    |
|  +--------------+  |
|  |React Web App.|  |
|  +--------------+  |
|                    |
+-+-+----------------+
  ^ |
  | | forex_restapi:8080
  | v
+-+-+-----------------+
|                     |
| WSGI server gateway |
| Flask REST API      |
|                     |
+-+-+-----------------+
  ^ |
  | | forex_database:5432
  | v
+-+-+-----------------+
| PostgeSQL database  |
+---------------------+
```

## Getting Started

### environment

This project uses the [fixer.io](https://fixer.io/) API. Locally you need to
export the variable: **FOREXIO_API_KEY** which must contain the API key provided
to you by fixer.io. This variable is picked up by docker-compose and made
available to the REST API.

Note that depending on the forex.io subscription you are using, not all
base currencies might be allowed to be selected for conversion, in case
the base_currency is not supported by your license, the application will
popup the message "base_currency_access_restricted".

### docker-compose

Build and start containers; in detached mode (runs in background).
```bash
docker-compose up --build -d
```

You can now open up `0.0.0.0:80` in your browser and here you will
see the web application.

Alternatively to the above, you can build and start with:
```bash
docker-compose -f docker-compose-dev.yml up --build -d
```

This starts the development friendly environment which contains the following
differences:

* Each container has the host's project root directory mounted at `/host`.
* The restapi will not be started, you must attach to the container
and start it manually.
* The restapi python build dependencies are not removed in the restapi image.
* The restapi python-dev dependencies are installed (needed to run the unit tests).

## Linting

### dashboard (react) web application

Lintend with eslint:

```
cd dashboard
yarn install
yarn lint
```

Note: The eslint messages "is missing in props validation" are false positives.

#### restapi python applciation

Linted with pylint:

```
pipenv install --dev
pipen run lint
```

## Testing

The restapi project contains unit tests which can be executed with `pipenv test`.

## Style guideline

### Python

* All code is autoformatted with: [black](https://github.com/psf/black).
* Docstrings are formatted according: [sphinx-napoleon](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/)

### Javascript

* All code is autoformatted with: [prettier](https://prettier.io/)

## Screenshots

![./screenshots/dashboard](./screenshots/dashboard.png)
![./screenshots/dashboard_create_trade](./screenshots/dashboard_create_trade.png)
