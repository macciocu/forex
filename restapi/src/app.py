import json
import requests

from flask import jsonify
from flask import request as flask_request
from gevent.pywsgi import WSGIServer

from forex import create_app
from forex.config import FOREXIO_API_KEY
from forex.tradehelper import TradeHelper

app = create_app()
trade_helper = TradeHelper()


@app.route("/api/get_trades", methods=["GET"])
def get_trades():
    return jsonify(trade_helper.get_trades_by_booking_date())


@app.route("/api/create_trade", methods=["POST"])
def create_trade():
    trade_helper.create_trade(flask_request.get_json())
    return jsonify(success=True)


@app.route("/api/get_currency_conversion_rate")
def get_currency_conversion_rate():
    """
    Args:
        ccy_from (str): From conversion rate (ISO 4217).
        ccy_to (str): To converstion rate (ISO 4217).

    Returns:
        Json with key given arg. `ccy_to` and arg. `ccy_to` exchange rate as value.

    """
    ccy_to = flask_request.args.get("ccy_to")
    ccy_from = flask_request.args.get("ccy_from")
    resp = requests.get(
        f"http://data.fixer.io/api/latest?access_key={FOREXIO_API_KEY}"
        + f"&base={ccy_from}&symbols={ccy_to}"
    )

    if resp.status_code != 200:
        print("[ERROR], Failed to receive conversion rate from fixer.io API.")
        return json("Service Unavailable"), 503

    return jsonify(resp.json())


if __name__ == "__main__":
    http_server = WSGIServer(("", 8080), app)
    http_server.serve_forever()
