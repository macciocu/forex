import time
import datetime
import pytest
import string

from forex import create_app
from forex.tradehelper import TradeHelper

# Sets up database connection
create_app()


@pytest.fixture
def trade_input():
    return {
        "sell_currency": "EUR",
        "sell_amount": 2,
        "buy_currency": "USD",
        "buy_amount": 6,
        "rate": 3,
    }


class TradeInDbStub:
    """Stub which represents `Trade.Models` present in the database."""

    sell_currency = "EUR"
    sell_amount = 2
    buy_currency = "USD"
    buy_amount = 6
    rate = 3
    date_booked = datetime.datetime(
        year=2020, month=12, day=10, hour=1, minute=2, second=3
    )

    def __init__(self, trade_id):
        self.id = trade_id


def test_serialize():
    assert TradeHelper().serialize(TradeInDbStub("TR0000001")) == {
        "id": "TR0000001",
        "sell_currency": "EUR",
        "sell_amount": 2,
        "buy_currency": "USD",
        "buy_amount": 6,
        "rate": 3,
        "date_booked": "2020/12/10 01:02:03",
    }


def test_create_trade(trade_input):
    th = TradeHelper()
    th.create_trade(trade_input)
    trade = th.get_trades_by_booking_date()[-1]
    for k in trade_input:
        assert trade.get(k) is not None
        assert trade_input[k] == trade[k]


def test_get_trades_ordering(trade_input):
    """Test that trades are received by descending booking_date."""
    th = TradeHelper()
    th.delete_all_trades()
    for i in range(3):
        th.create_trade(trade_input)
        time.sleep(2)
    trades = th.get_trades_by_booking_date()
    prev_ddhhmmss = 32000000  # init to max value (max. 31 days in a month)
    for trade in trades:
        date, time_ = trade["date_booked"].split(" ")
        ddhhmmss = int(date.split("/")[-1] + "".join(time_.split(":")))
        assert ddhhmmss < prev_ddhhmmss
        prev_ddhhmmss = ddhhmmss


def test_trade_id_format(trade_input):
    """Test that trade_id formatting / aplhanumeric incrementing works correctly."""
    th = TradeHelper()
    th.delete_all_trades()
    base = len(string.digits + string.ascii_uppercase + string.ascii_lowercase)
    n = base ** 2
    for i in range(n):
        if i == n - 1:
            time.sleep(2)  # ensures this will be the first entry in the query
        th.create_trade(trade_input)
    trades = th.get_trades_by_booking_date()
    assert trades[0]["id"] == "TR0000100"
