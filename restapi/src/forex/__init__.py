import time

from flask import Flask

from forex import config
from forex.models import db


def _wait_for_db_connection(timeout=10):
    try:
        db.session.query("1").from_statement("SELECT 1").all()
    except:  # noqa: E722
        if timeout <= 0:
            return False
        time.sleep(1)
        _wait_for_db_connection(timeout - 1)
    return True


def create_app():
    """
    Create flask application and establish connection with PostgeSQL datbase.

    """
    flask_app = Flask(__name__)
    flask_app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {
        "pool_pre_ping": True,
        "pool_recycle": 300,
    }
    flask_app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_CONNECTION_URI
    flask_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    flask_app.app_context().push()
    db.init_app(flask_app)

    if not _wait_for_db_connection():
        print("[ERROR] Failed to connect with database")
        return

    db.create_all()
    return flask_app
