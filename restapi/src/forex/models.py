import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()


class Trades(db.Model):
    __tablename__ = "trades"
    id = db.Column(db.String(9), primary_key=True, unique=True)
    sell_currency = db.Column(db.String(3))
    sell_amount = db.Column(db.Float)
    buy_currency = db.Column(db.String(3))
    buy_amount = db.Column(db.Float)
    rate = db.Column(db.Float)
    date_booked = db.Column(db.DateTime)
