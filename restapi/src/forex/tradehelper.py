import string

from datetime import datetime

from forex.models import db
from forex.models import Trades


class TradeHelper:
    """Helper class for working with `model.Trades` database entries."""

    def __init__(self):
        # We keep track of the last trades_id, as we need to increment it according
        # "TR" + 7 * alphanumeric format, e.g "TR0000001", "TR0000002" .. "TRzzzzzzz"
        last_trade = Trades.query.order_by(Trades.id.desc()).first()
        if last_trade is None:
            # Ensures first trade_id in database starts at TR0000001
            self._last_trade_id = "TR0000000"
        else:
            # Last trade_id currently present in database
            self._last_trade_id = last_trade.id

        # Holds Database entry `Trade.trade_id` chars for trade_id generation.
        # Alphanumerically sorted in order to ensure trade_ids are generated
        # in alphanumerically ascending order, and to easily retrieve the latest
        # trade_id from the database.
        self._chars = string.digits + string.ascii_uppercase + string.ascii_lowercase
        self._char_index_lookup = {}
        for i in range(len(self._chars)):
            self._char_index_lookup[self._chars[i]] = i

    def get_trades_by_booking_date(self):
        """
        Returns:
            Array of trades in serialized JSON format, sorted by booking date.

        """
        return [
            self.serialize(trade)
            for trade in Trades.query.order_by(Trades.date_booked.desc()).all()
        ]

    def serialize(self, trade):
        """
        Args:
            trade (Trades): Serialization subject.

        Returns:
            Serialized (JSON format) trade.

        """
        return {
            "id": trade.id,
            "sell_currency": trade.sell_currency,
            "sell_amount": trade.sell_amount,
            "buy_currency": trade.buy_currency,
            "buy_amount": trade.buy_amount,
            "rate": trade.rate,
            "date_booked": self._formatDate(trade.date_booked),
        }

    def create_trade(self, data):
        """
        Create `model.Trades` entry in database.

        Args: data (dict): Holds all key value pairs, except `id` and
            `date_booked` entry (will be computed automatically upon insertion),
            of `model.Trades`.

        """
        trade_id = self._increment_trade_id(self._last_trade_id)
        trade = Trades(
            id=trade_id,
            sell_currency=data["sell_currency"],
            sell_amount=data["sell_amount"],
            buy_currency=data["buy_currency"],
            buy_amount=data["buy_amount"],
            rate=data["rate"],
            date_booked=datetime.utcnow(),
        )
        db.session.add(trade)
        db.session.commit()
        self._last_trade_id = trade_id

    def delete_all_trades(self):
        """Delete all `model.Trades` entries."""
        db.session.query(Trades).delete()
        db.session.commit()
        self._last_trade_id = "TR0000000"

    def _increment_trade_id(self, trade_id):
        """
        Args:
            trade_id (str): Alphanumeric trade_id.

        Returns:
            Incremented `trade_id`; "TR" + 7 alphanumeric trade_id.

        """
        trade_id = list(trade_id[2:])
        for i in range(len(trade_id) - 1, -1, -1):
            if trade_id[i] == self._chars[-1]:
                trade_id[i] = self._chars[0]
            else:
                trade_id[i] = self._chars[self._char_index_lookup[trade_id[i]] + 1]
                break
        return "".join(["T", "R"] + trade_id)

    def _formatDate(self, date):
        """
        Args:
            date (datetime.datetime): `date` column queried from `models.Trades`;
                formatting subject.

        Returns:
            Given `date` formatted to "yyyy/mm/dd hh:mm:ss".

        """
        return (
            f"{date.year}/{date.month:02d}/{date.day:02d} "
            + f"{date.hour:02d}:{date.minute:02d}:{date.second:02d}"
        )
